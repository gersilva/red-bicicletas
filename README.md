# NodeJS - Express - MongoDB

Repositorio de proyectos del curso "Desarrollo del lado servidor: NodeJS, Express y MongoDB" de Coursera

Endpoints de la API:

GET - http://localhost:3000/api/v1/bicicletas - Listado de las bicicletas
POST - http://localhost:3000/api/v1/bicicletas - Crea una nueva bicicleta verificando que el body esté completo
PUT - http://localhost:3000/api/v1/bicicletas/:id - Edita la bicicleta con ID especificado en la ruta, verificando que el body esté completo
DELETE - http://localhost:3000/api/v1/bicicletas/:id - Elimina la bicicleta con ID especificado en la ruta
