window.addEventListener("load", () => {
    var mymap = L.map('mapid').setView([-32.8903045, -68.840296], 17);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoiZ2Vyc2lsdmE5NiIsImEiOiJja2V0NTRldmwxYmMwMnhtcmg3Ym1hcTV5In0.8HlUb8Mo_G6LMtTMlrRHrQ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'sk.eyJ1IjoiZ2Vyc2lsdmE5NiIsImEiOiJja2V0NTRldmwxYmMwMnhtcmg3Ym1hcTV5In0.8HlUb8Mo_G6LMtTMlrRHrQ'
    }).addTo(mymap);

    fetch("http://localhost:3000/api/v1/bicicletas")
        .then(result => result.json())
        .then(data => {
            data.bicicletas.forEach(bicicleta => {
                L.marker(bicicleta.ubicacion, {title: `Bicicleta ${bicicleta.id}`}).addTo(mymap);
            });
        });
});