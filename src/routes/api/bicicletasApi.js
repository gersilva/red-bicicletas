const express = require("express");
const router = express.Router();

const bicicletaApiController = require("../../controllers/api/bicicletaApi");

router.get("/", bicicletaApiController.list);

router.post("/", bicicletaApiController.store);

router.put("/:id", bicicletaApiController.update);

router.delete("/:id", bicicletaApiController.delete);

module.exports = router;