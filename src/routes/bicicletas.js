const express = require("express");
const router = express.Router();

const bicicletasController = require("../controllers/bicicletas");

router.get("/", bicicletasController.list);

router.get("/create", bicicletasController.create)
router.post("/", bicicletasController.store);

router.delete("/delete/:id", bicicletasController.delete);

router.get("/editar/:id", bicicletasController.edit);
router.put("/editar/:id", bicicletasController.update);

module.exports = router;