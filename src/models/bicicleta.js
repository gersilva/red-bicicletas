const Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = () => `id: ${this.id} | color: ${this.color} | modelo: ${this.modelo}`;

Bicicleta.allBicis = [];
Bicicleta.add = (aBici) => {
    Bicicleta.allBicis.push(aBici)
};
Bicicleta.findById = (id) => {
    const bici = Bicicleta.allBicis.find(x => x.id == id);
    if (bici) {
        return bici;
    } else {
        throw new Error(`No existe ninguna bicicleta con el id ${id}`);
    }
};
Bicicleta.removeById = (id) => {
    const bici = Bicicleta.findById(id);
    if (bici) {
        Bicicleta.allBicis.forEach((bicicleta, i) => {
            if (bicicleta.id == id) {
                Bicicleta.allBicis.splice(i, 1);
            }
        });
    } else {
        throw new Error("Error al eliminar la bicicleta");
    }
};
Bicicleta.update = (id, bicicleta) => {
    let bici = Bicicleta.findById(id);
    if (bici) {
        bici.id = parseInt(bicicleta.id);
        bici.color = bicicleta.color;
        bici.modelo = bicicleta.modelo;
        bici.ubicacion = bicicleta.ubicacion;
    } else {
        throw new Error("Error al editar la bicicleta");
    }
};

const a = new Bicicleta(1, "rojo", "urbana", [-32.888895, -68.842919]);
const b = new Bicicleta(2, "azul", "DH", [-32.891039, -68.843477]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;