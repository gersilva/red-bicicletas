const Bicicleta = require("../models/bicicleta");

const bicicletaController = {
    list: (req, res) => {
        res.render("bicicletas/index", {bicis: Bicicleta.allBicis});
    },
    create: (req, res) => {
        res.render("bicicletas/create");
    },
    store: (req, res) => {
        const bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [parseFloat(req.body.lat), parseFloat(req.body.lng)]);
        bici.ubicacion = [req.body.lat, req.body.lng];
        Bicicleta.add(bici);
        res.redirect("/bicicletas");
    },
    delete: (req, res) => {
        Bicicleta.removeById(req.params.id);
        res.redirect("/bicicletas");
    },
    edit: (req, res) => {
        const bici = Bicicleta.findById(req.params.id);
        res.render("bicicletas/edit", {bici});
    },
    update: (req, res) => {
        const bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [parseFloat(req.body.lat), parseFloat(req.body.lng)]);
        Bicicleta.update(req.params.id, bici);
        res.redirect("/bicicletas");
    }
};

module.exports = bicicletaController;