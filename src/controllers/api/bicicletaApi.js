const Bicicleta = require("../../models/bicicleta");

module.exports = {
    list: (req, res) => {
        res.status(200).json({
            bicicletas: Bicicleta.allBicis
        });
    },
    store: (req, res) => {
        if (!req.body.id || !req.body.color || !req.body.modelo || !req.body.lat || !req.body.lng) {
            return res.status(400).json({
                ok: false,
                message: "Todos los campos son obligatorios"
            });
        }
        if (req.body.id != undefined) {
            const biciExistente = Bicicleta.findById(req.body.id);
            if (biciExistente != undefined) {
                return res.status(400).json({
                    ok: false,
                    message: `La bicicleta con id ${req.body.id} ya existe`
                });
            }
        }
        const bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
        Bicicleta.add(bicicleta);
        res.status(201).json({
            ok: true,
            message: `Bicicleta ${bicicleta.id} agregada correctamente`
        });
    },
    update: (req, res) => {
        if (!req.body.id || !req.body.color || !req.body.modelo || !req.body.lat || !req.body.lng) {
            return res.status(400).json({
                ok: false,
                message: "Todos los campos son obligatorios"
            });
        }
        const bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
        Bicicleta.update(req.params.id, bicicleta);
        res.status(200).json({
            ok: true,
            message: `Bicicleta ${req.params.id} actualizada correctamente`
        })
    },
    delete: (req, res) => {
        Bicicleta.removeById(req.params.id);
        res.status(200).json({
            ok: true,
            message: `Bicicleta ${req.params.id} eliminada correctamente`
        })
    }
};